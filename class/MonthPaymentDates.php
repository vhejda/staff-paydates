<?php

/**
 * Class MonthPaymentDates
 * - Payment dates for specific month
 *
 * @author Vojtěch Hejda
 */
class MonthPaymentDates
{

    /**
     * @var \DateTime Month - first day in month, zero time
     */
    protected $month;

    /**
     * @var \DateTime Basic pay payment date
     */
    protected $basePaymentDate;

    /**
     * @var \DateTime Bonuses payment date
     */
    protected $bonusPaymentDate;

    /**
     * @param string $month mm format
     * @param string $year yyyy format
     */
    public function __construct($month, $year)
    {
        $this->month = new DateTime("$year-$month");
    }

    /**
     * @return string formatted name of the month and year
     */
    public function getName()
    {
        return $this->month->format("F Y");
    }

    /**
     * @return \DateTime
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @return \DateTime Basic pay payment date
     */
    public function getBasePaymentDate(): DateTime
    {
        return $this->basePaymentDate;
    }

    /**
     * @param \DateTime $basePaymentDate
     * @return $this
     * @throws \Exception
     */
    public function setBasePaymentDate(DateTime $basePaymentDate)
    {
        // Is the payment date in right month?
        if ($this->month->format("Y-m") !== $basePaymentDate->format("Y-m")) {
            throw new Exception(
                "Base pay payment date has to be in " . $this->month->format("F Y") . ", "
                . $basePaymentDate->format("F Y") . " given."
            );
        }

        $this->basePaymentDate = $basePaymentDate;
        return $this;
    }

    /**
     * @return \DateTime Bonuses payment date
     */
    public function getBonusPaymentDate(): DateTime
    {
        return $this->bonusPaymentDate;
    }

    /**
     * @param \DateTime $bonusPaymentDate
     * @return $this
     * @throws \Exception
     */
    public function setBonusPaymentDate(DateTime $bonusPaymentDate)
    {
        // Is the payment date in right month?
        if ($this->month->format("Y-m") !== $bonusPaymentDate->format("Y-m")) {
            throw new Exception(
                "Bonus payment date has to be in " . $this->month->format("F Y") . ", "
                . $bonusPaymentDate->format("F Y") . " given."
            );
        }

        $this->bonusPaymentDate = $bonusPaymentDate;
        return $this;
    }

}
