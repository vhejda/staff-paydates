<?php

/**
 * Class CsvWriter
 * - Encapsulates CSV writing functionality
 *
 * @author Vojtěch Hejda
 */
class CsvWriter
{
    protected $handle;

    /**
     * @param string $output_file path to output file
     * @param $rewrite
     */
    public function __construct($output_file, $rewrite = false)
    {
        if ($rewrite and file_exists($output_file)) {
            unlink($output_file);
        }
        $this->handle = fopen($output_file, "w");
    }

    /**
     * @param array $columns
     */
    public function addLine($columns)
    {
        fputcsv($this->handle, $columns);
    }

}
