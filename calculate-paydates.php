<?php
include "class/MonthPaymentDatesCalculator.php";
include "class/CsvWriter.php";

$now = new \DateTime();
$number_of_months = 12;      // Number of month to calculate

// --------------------------------------------
//  CSV
// --------------------------------------------
$output_file = __DIR__ . "/csv/paydates-from-" . $now->modify("+ 1 month")->format("m-Y") .
    "-to-" . $now->modify("+ " . ($number_of_months-1) . " months")->format("m-Y") . ".csv";
$writer = new CsvWriter($output_file, true);
// Headings:
$writer->addLine(array(
    "Month name",
    "Base payment date",
    "Bonus payment date"
));

// --------------------------------------------
//  Calculating
// --------------------------------------------
$calculator = new MonthPaymentDatesCalculator();

/** @var array | MonthPaymentDates[] $calculated_months Next 12 months */
$calculated_months = $calculator->calculateNext($number_of_months)->getCalculatedMonths();

foreach ($calculated_months as $calculated_month) {
    $writer->addLine(array(
        $calculated_month->getName(),
        $calculated_month->getBasePaymentDate()->format("l, j.m.Y"),
        $calculated_month->getBonusPaymentDate()->format("l, j.m.Y")
    ));
}
