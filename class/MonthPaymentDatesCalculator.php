<?php

include "MonthPaymentDates.php";

/**
 * Class MonthPaymentDatesCalculator
 * - Creates MonthPaymentDates objects with calculated payment dates
 *
 * @author Vojtěch Hejda
 */
class MonthPaymentDatesCalculator
{

    /**
     * @var array|MonthPaymentDates
     */
    private $calculatedMonths = array();

    /**
     * Returns calculated months
     *
     * @return array|MonthPaymentDates
     */
    public function getCalculatedMonths()
    {
        return $this->calculatedMonths;
    }

    /**
     * @param \DateTime $month - first day in month, zero time
     */
    private function calculate($month) {
        try {
            $calculated_month = new MonthPaymentDates($month->format("m"), $month->format("Y"));

            /* Calculation of Base pay date:
             *      "Basic pay is paid on the last working day of the month (Mon-Fri).
             *      So if the last day of January is the 31 st , and this is a Saturday,
             *      the payment date is Friday the 30 th . The same logic applies to Sunday."
             */
            $base_pay_day = new \DateTime($month->format('Y-m-t'));
            $weekday_number = $base_pay_day->format("N");
            if ($weekday_number > 5) {
                $base_pay_day->modify("-" . ($weekday_number - 5) . " days");
            }
            $calculated_month->setBasePaymentDate($base_pay_day);

            /* Calculation of Bonus pay date:
             *      "On the 12th of every month bonuses are paid for the previous month, unless that day
             *      is a weekend. In that case, they are paid the first Tuesday after the 12th."
             */
            $bonus_pay_day = clone $month;
            $bonus_pay_day->modify('+ 11 days');
            $weekday_number = $bonus_pay_day->format("N");
            if ($weekday_number > 5) {
                $bonus_pay_day->modify("next tuesday");
            }
            $calculated_month->setBonusPaymentDate($bonus_pay_day);

        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            exit(1);
        }

        $this->calculatedMonths[] = $calculated_month;
    }

    /**
     * Calculates paydates for next $number months
     *
     * @param int $number
     * @return $this
     */
    public function calculateNext($number) {
        // Calculate next $number of months:
        $month = new \DateTime(date("Y-m"));
        while ($number > 0) {
            $month->modify("+ 1 month");
            $this->calculate($month);
            $number--;
        }

        return $this;
    }

}
